// setup the dependencies
const express = require("express");
const router = express.Router();
// import the controller file.
const courseController = require("../controllers/course");
const auth = require("../auth");
const { route } = require("./user");

// route to get all courses.
router.get("/", (req, res) => {
    courseController.getCourses().then(resultFromController => res.send(resultFromController));
});

// route to get specific course.
// courseId here is called a "wildcard" and its value is anything that is added at the end of localhost:4000/courses.
// e.g. localhost:4000/courses/dog = the value of our wildcard is "dog".
router.get("/:courseId", (req, res) => {
    courseController.getSpecific(req.params.courseId).then(resultFromController => res.send(resultFromController));

    // console.log(req.params);
    // res.send("Hello");
});

// route to create a new course.
// when a user sends a specific method to a specific endpoint (in this case a POST request to our /courses endpoint) the code within this route will be run.
router.post("/", auth.verify, (req, res) => {
    // auth.verify here is something called "middleware". middleware is any function that must first be resolved before any success
    // show in the console the request body.
    // console.log(req.body);

    // console.log(auth.decode(req.headers.authorization));
    // res.send("Hello");

    if (auth.decode(req.headers.authorization).isAdmin) {
        courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send(false); 
    };
    
});

// route to update a single course.
router.put("/:courseId", auth.verify, (req, res) => {
    if (auth.decode(req.headers.authorization).isAdmin) {
        courseController.updateCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send(false);
    };
    
});

// router to archive course
router.delete("/:courseId", auth.verify, (req, res) => {
    if (auth.decode(req.headers.authorization).isAdmin) {
        courseController.archiveCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send(false);
    };
    
});

// export the router
module.exports = router;
