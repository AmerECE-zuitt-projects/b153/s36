const Course = require("../models/course");

module.exports.getCourses = () => {
    // find all courses that are active then put the result in a variable called result and return that result
    return Course.find({}).then(result => {
        return result;
    });
};

module.exports.getSpecific = (courseId) => {
    // findById() is like findOne() except it can only find by id.
    return Course.findById(courseId).then(result => {
        return result
    });
};

module.exports.addCourse = (body) => {
    // create a new object called newCourse based on our Course model. each od this fields values will come from the request body.
    let newCourse = new Course({
        name: body.name,
        description: body.description,
        price: body.price
    });

    // use save() to save newCourse object to our database. if savings is not successful, an error message will be contained inside of the error parameter passed to .then()
    // if the error parameter has a value, then it will render true in our if statement, and the function will return false.
    // \if saving is successful, the error parameter will be empty, and thus false. this will cause our else statement to be run instead and the function will return true.
    // we also pass the req.body to addCourse as part of the data that it needs.
    // once addCourse has resolved. then can send whatever it returns (true or false) as its response.
    return newCourse.save().then((course, error) => {
        if (error) {
            return false;
        }else {
            return true;
        }
    });
};

module.exports.updateCourse = (courseId, body) => {
    let updateCourse = {
        name: body.name,
        description: body.description,
        price: body.price
    };

    // use findByIdAndUpdate to find the course we want to update and pass the updatedCourse object as our new course data.
    return Course.findByIdAndUpdate(courseId, updateCourse).then((course, error) => {
        if (error) {
            return false;
        } else {
            return true;
        };
    });
};

module.exports.archiveCourse = (courseId) => {
    let archiveCourse = {
        isActive: false
    };

    return Course.findByIdAndUpdate(courseId, archiveCourse).then((course, error) => {
        if (error) {
            return false;
        } else {
            return true;
        };
    });
};